# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-emoji
_pkgorig=emoji
pkgver=2.12.0
pkgrel=0
pkgdesc="Emoji terminal output for Python"
url="https://github.com/carpedm20/emoji"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-typing-extensions"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest-xdist"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/carpedm20/emoji/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -k 'not test_zwj_replace and not test_zwj_remove'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
6392617e57ccd0168cb896723e9c187b9674b91ba80b6a9fb45e8baa186b77237fb448866c893869dc041c29fad228b87349db52223a52e7708f72572729d90b  py3-emoji-2.12.0.tar.gz
"
